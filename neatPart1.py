# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 18:27:36 2016

@author: alex

NEAT functionality
"""


import os
import sys
import time
import random as rnd
import cv2
import numpy as np
import pickle as pickle
import MultiNEAT as NEAT
from MultiNEAT import EvaluateGenomeList_Serial
from MultiNEAT import GetGenomeList, ZipFitness
import convert_to_SpiNNaker_network as conv_spiNN
from concurrent.futures import ProcessPoolExecutor, as_completed
import copy
import plotNN

class Neat:
    def __init__(self,NIndividuals):
        
        self.params = NEAT.Parameters()
        self.params.PopulationSize = NIndividuals
        
        self.params.OverallMutationRate = 0.25#probability of mutation after crossover# def 0.25
        self.params.MutateWeightsProb = 0.9#def=0.9

        self.params.RecurrentProb = 0.25#def=0.25
        
        self.params.WeightMutationMaxPower = 4.0#max change of a mutation
        
        self.params.WeightMutationRate = 0.9#def=1.0

        self.params.MutateAddNeuronProb = 0.2#def=0.01
        self.params.MutateAddLinkProb = 0.2#def=0.03
        self.params.MutateRemLinkProb = 0.2#def=0.0
        self.params.MutateRemSimpleNeuronProb = 0.2#def=0.0
        
        self.params.EliteFraction = 0.2 #fracion of individuals to be copied unchanged ??per species??#def=0.01

        """
            // If true, this enables dynamic compatibility thresholding
            // It will keep the number of species between MinSpecies and MaxSpecies
        """
        self.params.DynamicCompatibility = True
        self.params.WeightDiffCoeff = 4.0
        
        self.params.YoungAgeTreshold = 15#young species get a fitness boost
        self.params.SpeciesMaxStagnation = 15
        self.params.OldAgeTreshold = 30#old species can get fitness penalty, but not by default
#       // Multiplier that penalizes old species.
#       // Make sure it is <= 1.0 to avoid confusion.
        self.OldAgePenalty = 0.8
        self.params.MinSpecies = 3
        self.params.MaxSpecies = 10
        self.params.RouletteWheelSelection = False

#        // Compatibility treshold
        self.CompatTreshold = 2.0;#def=5.0
    
#        // Minumal value of the compatibility treshold
        self.MinCompatTreshold = 0.2;
    
#        // Modifier per generation for keeping the species stable
        self.CompatTresholdModifier = 0.3;
    
#        // Per how many generations to change the treshold
#        // (used in generational mode)
        self.CompatTreshChangeInterval_Generations = 1;
    

        self.params.WeightReplacementMaxPower = 3.0#def=1.0
        self.params.MutateWeightsSevereProb = 0.2
        
        self.params.MaxWeight = 15
        
        
        self.params.MinActivationA  = 4.9
        self.params.MaxActivationA  = 4.9
        
        self.params.ActivationFunction_SignedSigmoid_Prob = 0.0
        self.params.ActivationFunction_UnsignedSigmoid_Prob = 1.0
        self.params.ActivationFunction_Tanh_Prob = 0.0
        self.params.ActivationFunction_SignedStep_Prob = 0.0
        
        self.params.CrossoverRate = 0.75  # if no crossover, 100% mutation (?)
        """
            // Probability for a baby to result from Multipoint Crossover when mating. 1.0 = 100%
            // The default is the Average mating.
        """
        self.params.MultipointCrossoverRate = 0.4
        self.params.SurvivalRate = 0.2
        
    
    def initialization(self, Ninputs, Noutputs):
        """ 
        returns initial random network of Ninputs input neurons and Noutputs output neurons.
        Fully connected and random weights.
        """
        params = self.params
    
        randint = np.random.randint(0,10000000)
        
        #Genome(ID, NumInputs, NumHidden, NumOutputs, FS_NEAT (feature selective, choose inputs automatically), \\
            #ActivationFunction OutputActType, ActivationFunction HiddenActType, SeedType, Parameters)
        g = NEAT.Genome(0, Ninputs, 0, Noutputs, False, NEAT.ActivationFunction.UNSIGNED_SIGMOID, NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, params)
        
        #Population(genome, parameters, RandomizeWeights, RandomizationRange, RNG_seed)    
        neatPop = NEAT.Population(g, params, True, 1.0, randint)
          
#       pop.RNG.Seed(i) D   ont know what this does, seems unhelpfull, adding unconnected output neurons to otherwise fine networks
    
        AllNetworkStructures, AllConnectivity = self.extract_connectivity(neatPop)
                    
        spiNN_populations=[sum(np.array(AllNetworkStructures)[:,x]) for x in range(3)]
        
        #cm = plotNN.get_cm(net)
        spiNN_projections = self.convert_to_spiNN(AllConnectivity,AllNetworkStructures)
        
        Nindividuals = len(AllNetworkStructures)
    
        return spiNN_populations, spiNN_projections, Nindividuals, neatPop


    def breed(self, neatPop, fitness):
        """  
        given previous generation and fitness, create new generation
        
        outputs:
            spiNN_populations = [NumberInputNeurons, NumberHiddenNeurons, NumberOutputNeurons, NumberIndividuals]
            spiNN_projections = [proj_input_hidden,
                                 proj_input_output,
                                 proj_hidden_hidden,
                                 proj_hidden_output,
                                 proj_output_hidden]
                    e.g.:   proj_input_hidden = [ (sourceNeuronIdx, TargetNeuronIdx, Weight),
                                                  (               ,               ,        ), 
                                                    ... 
                                                ]
        """
    
        genome_list = NEAT.GetGenomeList(neatPop)
        NEAT.ZipFitness(genome_list, list(fitness))#just sets the fitness value of each genome
        print "neatPop.Parameters.EliteFraction : ", neatPop.Parameters.EliteFraction
        print "neatPop.Parameters.MaxSpecies : ", neatPop.Parameters.MaxSpecies
        print "neatPop.Parameters.MutateWeightsProb: ", neatPop.Parameters.MutateWeightsProb
        print "neatPop.Parameters.MutateAddNeuronProb : ", neatPop.Parameters.MutateAddNeuronProb
        k=0
        for species in neatPop.Species:
            print "=================fitnesses species %i (ID: %i, age %i) :" %(k,species.ID(),species.Age())
            k = k+1 
            fs=[]
            for indiv in species.Individuals:
                fs.append(int(indiv.GetFitness()))
            print fs
        
        neatPop.Epoch()
          
        AllNetworkStructures, AllConnectivity = self.extract_connectivity(neatPop)
                    
        spiNN_populations=[sum(np.array(AllNetworkStructures)[:,x]) for x in range(3)]
        
        
        #cm = plotNN.get_cm(net)
        spiNN_projections = self.convert_to_spiNN(AllConnectivity,AllNetworkStructures)
        
        Nindividuals = len(AllNetworkStructures)
    
        return spiNN_populations, spiNN_projections, Nindividuals, neatPop

        
    def extract_connectivity(self,pop):
        """
            param pop = _MultiNEAT.Population instance
            outputs:
                AllConnectivity
                ALLNetworkStructures
        """
        net=NEAT.NeuralNetwork()
        #neuron.type
        AllNetworkStructures = []
        AllConnectivity = []
        for spec in pop.Species:
            for ind in spec.Individuals:
                #clear net and rebuild        
            
                net.Clear()
                ind.BuildPhenotype(net)
                
                #collect layer structure [Input,Hidden,Output]
                Total = len(net.neurons)
                In = net.NumInputs()
                Out = net.NumOutputs()
                AllNetworkStructures.append([In, Total-In-Out, Out])

                #collect individuals connections [source,target,weight]
                ind_connections = []
                conn_input_hidden = []
                conn_input_output = []
                conn_hidden_hidden = []
                conn_hidden_output = []
                conn_output_hidden = []
                #loop over each connection in the network
                for i in range(len(net.connections)):
                    conn = net.connections[i]
                    
                    #connections from input layer 
                    if conn.source_neuron_idx in range(In):
                        #connections from input layer to output layer | input->output                
                        if conn.target_neuron_idx in range(In,In+Out):
                            conn_input_output.append([conn.source_neuron_idx, conn.target_neuron_idx,conn.weight])
                        #connections from input layer to hidden layer | input->hidden
                        if conn.target_neuron_idx in range(In+Out,Total):
                            conn_input_hidden.append([conn.source_neuron_idx, conn.target_neuron_idx,conn.weight])
                        #connections from input layer to input layer  | input->input    | seems not to occur
                    
                    #connections from output layer
                    elif conn.source_neuron_idx in range(In,In+Out):
                        #connections from output layer to hidden layer| output->hidden                
                        if conn.target_neuron_idx in range(In+Out,Total):
                            conn_output_hidden.append([conn.source_neuron_idx, conn.target_neuron_idx,conn.weight])
                        #connections from output layer to input layer | output->input   | seems not to occur
                        #connections from output layer to output layer| output->output  | seems not to occur
        
                    #connections from hidden layer
                    elif conn.source_neuron_idx in range(In+Out,Total):
                        #connections from hidden layer to output layer| hidden->output                
                        if conn.target_neuron_idx in range(In,In+Out):
                            conn_hidden_output.append([conn.source_neuron_idx, conn.target_neuron_idx,conn.weight])
                        #connections from hidden layer to hidden layer| hidden->hidden
                        if conn.target_neuron_idx in range(In+Out,Total):
                            conn_hidden_hidden.append([conn.source_neuron_idx, conn.target_neuron_idx,conn.weight])
                        #connections from hidden layer to input layer | hidden->input   | seems not to occur
        
                ind_connections = [np.array(conn_input_hidden),
                                   np.array(conn_input_output),
                                   np.array(conn_hidden_hidden),
                                   np.array(conn_hidden_output),
                                   np.array(conn_output_hidden)]
                                   
                AllConnectivity.append(ind_connections)
        
        return AllNetworkStructures, AllConnectivity

    def convert_to_spiNN(self,AllConnectivity,AllNetworkStructures):
        """
        Convert population of networks into a single network uploadable to spiNNaker
        
        Input params:
            AllNetworkStructures contains number of neurons of each ind. network layers:
                ind_structure = [Input,Hidden,Output]
        
            AllConnectivity contains all connections per ind:
                ind_conns = [   np.array(conn_input_hidden),
                                np.array(conn_input_output),
                                np.array(conn_hidden_hidden),
                                np.array(conn_hidden_output),
                                np.array(conn_output_hidden)]
        Output params:
            Projections contains the projections as lists of tuples that can be given to spiNNaker:
                Projections = [proj_input_hidden,
                               proj_input_output,
                               proj_hidden_hidden,
                               proj_hidden_output,
                               proj_output_hidden]
        """    
        spiNN_network= copy.deepcopy(AllConnectivity)
        
        Count_input_pop = 0
        Count_hidden_pop = 0
        Count_output_pop = 0
        #loop over each individuals connections and change neuron indexes
        #the many error catching is to deal with empty lists...
        for ind_conns, ind_structure in zip(spiNN_network,AllNetworkStructures):
            
            In, Out = ind_structure[0], ind_structure[2]
            #increment the input neurons with Count_input_pop
            try:
                ind_conns[0][:,0] += Count_input_pop
            except IndexError:
                pass
            try:
                ind_conns[1][:,0] += Count_input_pop
            except IndexError:
                pass
            #increment Counter
            Count_input_pop += ind_structure[0]
            
            #increment the hidden neurons with Count_hidden_pop
            try:
                ind_conns[0][:,1] += Count_hidden_pop-(In+Out)
            except IndexError:
                pass
            try:
                ind_conns[2][:,0] += Count_hidden_pop-(In+Out)
            except IndexError:
                pass
            try:
                ind_conns[2][:,1] += Count_hidden_pop-(In+Out)
            except IndexError:
                pass
            try:
                ind_conns[3][:,0] += Count_hidden_pop-(In+Out)
            except IndexError:
                pass
            try:
                ind_conns[4][:,1] += Count_hidden_pop-(In+Out)
            except IndexError:
                pass
            #increment counter
            Count_hidden_pop += ind_structure[1]
            
            #increment the output neurons wirt Count_output_pop
            try:
                ind_conns[1][:,1] += Count_output_pop-(In)
            except IndexError:
                pass
            try:
                ind_conns[3][:,1] += Count_output_pop-(In)
            except IndexError:
                pass
            try:
                ind_conns[4][:,0] += Count_output_pop-(In)
            except IndexError:
                pass
            #increment counter
            Count_output_pop += ind_structure[2]
        
        
        """    
        Projections = [proj_input_hidden,
                      proj_input_output,
                      proj_hidden_hidden,
                      proj_hidden_output,
                      proj_output_hidden]
        """
        Projections = list()
        for i in range(5):
            try:    
                proj = np.vstack(([x[i] for x in spiNN_network if len(x[i])>0]))
                Projections.append(proj)
            except ValueError as e:     #in case of empty np.arrays
                if e.message == "need at least one array to concatenate":
                    Projections.append(np.array([]))
                else:
                    raise
           
        
        for i in range(len(Projections)):
            #add delay column (fixed to 1ms), if array not empty
            if len(Projections[i]>0):
                c = np.ones((Projections[i].shape[0],1))*5
                Projections[i] = np.hstack((Projections[i],c))        
            #reformat to list of tuples, that can be understood by spyNNaker 
            Projections[i] = [tuple(x) for x in Projections[i]]
            
        return Projections
        

