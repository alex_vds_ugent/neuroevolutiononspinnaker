# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 18:37:15 2016

@author: alex
"""
import random


class processNeatPopTextFile:

    def __init__(self,folder,fileName,prevNInputs):
        self.prevNInputs=prevNInputs
        f = open(folder+'/'+fileName,"r")
        text = [line.strip() for line in f]
        f.close()
        self.outText = []
        for idx, lin in enumerate(text):
            if "NextInnovNum" in lin:
                NR = lin[lin.find(' ')+1:]
                newNR = int(NR)+16
                newLin = lin[:lin.find(' ')+1]+str(newNR)
                self.outText.append(newLin)
                print newLin 
            elif "NextNeuronID" in lin:
                NR = lin[lin.find(' ')+1:]
                newNR = int(NR)+4
                newLin = lin[:lin.find(' ')+1]+str(newNR)
                self.outText.append(newLin)
                print  newLin
            elif "Innovation " in lin:
                p = lin.partition(' ')
                newLin = p[0]+p[1]
                p2=p[2].partition(' ')
                newLin = newLin+p2[0]+p2[1]
                p3=p2[2].partition(' ')
                newLin = newLin + p3[0]+p3[1]
                p4=p3[2].partition(' ')
                N1 = int(p4[0])
                if N1<=self.prevNInputs:
                    newLin= newLin + str(N1) + p4[1]
                elif N1>self.prevNInputs:
                    newLin= newLin + str(N1+4) + p4[1]                    
                p5=p4[2].partition(' ')
                newLin = newLin + str(int(p5[0])+4) + p5[1] + p5[2]
                self.outText.append(newLin)
                print newLin
            elif "InnovationDatabaseEnd" in lin:
                self.prevID = int(text[idx-1].partition(' ')[2].partition(' ')[0])
                newLins = []
                newLins.append("Innovation "+str(self.prevID+1)+" 1 9 13 0 -1") 
                newLins.append("Innovation "+str(self.prevID+2)+" 1 9 14 0 -1") 
                newLins.append("Innovation "+str(self.prevID+3)+" 1 9 15 0 -1")
                newLins.append("Innovation "+str(self.prevID+4)+" 1 9 16 0 -1") 
                newLins.append("Innovation "+str(self.prevID+5)+" 1 10 13 0 -1")
                newLins.append("Innovation "+str(self.prevID+6)+" 1 10 14 0 -1")
                newLins.append("Innovation "+str(self.prevID+7)+" 1 10 15 0 -1")
                newLins.append("Innovation "+str(self.prevID+8)+" 1 10 16 0 -1")
                newLins.append("Innovation "+str(self.prevID+9)+" 1 11 13 0 -1")
                newLins.append("Innovation "+str(self.prevID+10)+" 1 11 14 0 -1")
                newLins.append("Innovation "+str(self.prevID+11)+" 1 11 15 0 -1")
                newLins.append("Innovation "+str(self.prevID+12)+" 1 11 16 0 -1")
                newLins.append("Innovation "+str(self.prevID+13)+" 1 12 13 0 -1")
                newLins.append("Innovation "+str(self.prevID+14)+" 1 12 14 0 -1")
                newLins.append("Innovation "+str(self.prevID+15)+" 1 12 15 0 -1")
                newLins.append("Innovation "+str(self.prevID+16)+" 1 12 16 0 -1")
                newLin = "InnovationDatabaseEnd"
                for x in newLins:
                    self.outText.append(x)
                self.outText.append(newLin)
                for x in newLins:
                    print x
                print newLin
            elif "Neuron " in lin:
                p = lin.partition(' ')
                p2= p[2].partition(' ')
                neuronID = int(p2[0])
        
                if neuronID < self.prevNInputs:
                    newLin = lin
                    self.outText.append(newLin)
                    print newLin
                if neuronID == self.prevNInputs:
                    newLin1 = lin
                    newLin2 = "Neuron 9 1 0,00000000 1 0,00000000 0,00000000 0,00000000 0,00000000"
                    newLin3 = "Neuron 10 1 0,00000000 1 0,00000000 0,00000000 0,00000000 0,00000000"
                    newLin4 = "Neuron 11 1 0,00000000 1 0,00000000 0,00000000 0,00000000 0,00000000"
                    newLin5 = "Neuron 12 1 0,00000000 1 0,00000000 0,00000000 0,00000000 0,00000000"
                    self.outText.append(newLin1)
                    self.outText.append(newLin2)
                    self.outText.append(newLin3)
                    self.outText.append(newLin4)
                    self.outText.append(newLin5)
                    print newLin1
                    print newLin2
                    print newLin3
                    print newLin4
                    print newLin5
                if neuronID >self.prevNInputs:
                    newLin = "Neuron "+str(neuronID+4)+" "+p2[2]
                    self.outText.append(newLin)
                    print newLin
            elif "Link " in lin:
                p = lin.partition(' ')[2].partition(' ')
                p2 = p[2].partition(' ')        
                N1 = int(p[0])
                N2 = int(p2[0])
                if N1 >self.prevNInputs:
                    N1 = N1+4
                if N2 >self.prevNInputs:
                    N2=N2+4
                newLin = "Link "+str(N1)+' '+str(N2)+' '+p2[2]
                self.outText.append(newLin)
                print newLin
                
            elif "GenomeEnd" in lin:
                newLins=[]
                newLins.append("Link 9 13 "+str(self.prevID+1)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 9 14 "+str(self.prevID+2)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 9 15 "+str(self.prevID+3)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 9 16 "+str(self.prevID+4)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 10 13 "+str(self.prevID+5)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 10 14 "+str(self.prevID+6)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 10 15 "+str(self.prevID+7)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 10 16 "+str(self.prevID+8)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 11 13 "+str(self.prevID+9)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 11 14 "+str(self.prevID+10)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 11 15 "+str(self.prevID+11)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 11 16 "+str(self.prevID+12)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 12 13 "+str(self.prevID+13)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 12 14 "+str(self.prevID+14)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 12 15 "+str(self.prevID+15)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLins.append("Link 12 16 "+str(self.prevID+16)+' 0 '+str(random.random()*2-1).replace('.',',')[:10])
                newLin = "GenomeEnd"
                for x in newLins:
                    self.outText.append(x)
                self.outText.append(newLin)
                for x in newLins:
                    print x
                print newLin
            else:
                self.outText.append(lin)
                print lin
        print self.outText
        with open(folder+'/'+fileName+'AddedInputs',"w") as outF:
            [outF.write(line+"\n") for line in self.outText]
        