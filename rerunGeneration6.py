# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:40:29 2016

@author: alex

uses interface 15 (8 inputs)
"""


import interface17
import numpy as np
import neatPart1
import os
import visualization
import time
import copy
import cPickle
import MultiNEAT

class runGeneration:
    def __init__(self,experimentName,NIndividuals,NRuns,NFrames,inputGenerationDir):
        self.experimentName = experimentName
        self.Neat = neatPart1.Neat(NIndividuals)
        self.NFrames = NFrames
#        self.repeats = repeats
        repeats =1
        #next three only used for printing experiment info to terminal        
        self.Nerrors = 0
        self.max_scores = list()
        self.avg_scores= list()
        #create experiment directory
        try:
            saveGameDir=("/home/alex/Documents/saveGames/"+ self.experimentName)
            os.mkdir(saveGameDir)
        except OSError:
            print "----------------------------------------------------------"
            print "Warning: using an existing folder to save experiment data"
            print "----------------------------------------------------------"            
            pass
        try:
            neatPop = MultiNEAT.Population(inputGenerationDir+"/neatPop")
            print "loaded neatpop succesfully"
        except :
            try:
                neatPop = MultiNEAT.Population(inputGenerationDir)
            except:
                print 'problem loading neatPop'
                raise
        
        print 'kip'
        AllNetworkStructures, AllConnectivity = self.Neat.extract_connectivity(neatPop)
        spiNNprojections = self.Neat.convert_to_spiNN(AllConnectivity,AllNetworkStructures)
        
        Nindividuals = len(AllNetworkStructures)
        spiNNpopulations=[sum(np.array(AllNetworkStructures)[:,x]) for x in range(3)]
        
        
        
        
        
        for x in range(NRuns):
            saveGameDir = self.getDir()
            os.mkdir(saveGameDir)
            
            self.saveNets(spiNNpopulations, spiNNprojections, Nindividuals, neatPop,saveGameDir)                    
            sim = interface17.interface(spiNNpopulations, spiNNprojections, Nindividuals, self.NFrames, repeats, saveGameDir)
            
            #wait until all pacman game results are collected
            while True:
                error = sim.getError()#@Alexander: delete this line, for debugging purposes only
                
                if sim.getSimFinished() and not(sim.getErrorCatched()):
                    fitness = sim.getScores() #will the outer While statement
                    print "gotfirstfitness"
                    break
            
    def getDir(self):
        localTime = time.localtime()[0:6]#used to make saveGame folder with timestamp
        Dir = "/home/alex/Documents/saveGames/"+ self.experimentName + "/" +"-".join([str(t) for t in localTime])
        return Dir

    def saveNets(self,spiNNpopulations, spiNNprojections, Nindividuals, neatPop, saveGameDir):
        #save spiNNnetworks    
        f = file(saveGameDir+"/spiNNnetwork", 'w')
        components = {'spiNNpopulations': spiNNpopulations, 'spiNNprojections': spiNNprojections, 'Nindividuals': Nindividuals}
        cPickle.dump(components, f)
        f.close()
        #save neat networks
        fileName = saveGameDir+"/neatPop"
        neatPop.Save(fileName)
        #save spiNN network figure
        visualization.visualize_spiNN_NN(copy.copy(spiNNprojections),saveGameDir+"/spiNNakerNetwork",size=None)

        return
