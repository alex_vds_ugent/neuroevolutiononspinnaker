# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 16:00:25 2016

@author: alex

interface 15 & 16 & 17

"""


import interface17
import numpy as np
import neatPart1
import os
import visualization
import time
import copy
import cPickle
import simplejson
import MultiNEAT

class evoLoop:
    def __init__(self,experimentName,NIndividuals,maxGens,NFrames,repeats=1,inputGenerationDir=None):
        """
        Start an evolutionary experiment with simulations on SpiNNaker. Creates random initial populations except if inpuGeneration is given.
        :param experimentName: String. Used for storing saveData (in current directory)
        :param NIndividuals: Int, Size of population
        :param maxGens: Int, Stopping criterium
        :param NFrames: Int, Number of frames simulated per pacman game
        :param repeats: Int, Number of pacman games (trials) used to assess a network
        :param inputGenerationDir: String, optional, If you don't want to start from a random generation but from a previously saved one
        """
        self.experimentName = experimentName
        self.Neat = neatPart1.Neat(NIndividuals)
        self.NFrames = NFrames
        self.repeats = repeats
        #next three only used for printing experiment info to terminal        
        self.Nerrors = 0
        self.max_scores = list()
        self.avg_scores= list()
        #create experiment directory
        try:
            os.mkdir("/home/alex/Documents/saveGames/"+ self.experimentName)
        except OSError:
            print "----------------------------------------------------------"
            print "Warning: using an existing folder to save experiment data"
            print "----------------------------------------------------------"            
            pass
        
        #if no inputGeneration is given, create a fresh random generation and run experiment
        if inputGenerationDir==None:
            neatPop, fitness = self.initialize()
            self.runLoop(neatPop,fitness,maxGens)
        #else load population and fitnesses, then run experiment
        else:
            try:
                neatPop = MultiNEAT.Population(inputGenerationDir+"/neatPop")
                with open(inputGenerationDir+"/scores","r") as openfile:
                    fitness = np.array(simplejson.load(openfile)['scores'])
                try:
                    fitness=np.average(fitness,axis=1)    
                except:
                    pass
                if np.min(fitness)<0:#make all fitnesses nonnegative
                    fitness = fitness + np.abs(np.min(fitness))
            except Exception:
                print "problem loading neatPop and/or scores from %s" %(inputGenerationDir)
                raise
            self.runLoop(neatPop,fitness,maxGens)
        
    def getDir(self):
        localTime = time.localtime()[0:6]#used to make saveGame folder with timestamp
        Dir = "/home/alex/Documents/saveGames/"+ self.experimentName + "/" +"-".join([str(t) for t in localTime])
        return Dir
    
    def saveNets(self,spiNNpopulations, spiNNprojections, Nindividuals, neatPop, saveGameDir):
        #save spiNNnetworks    
        f = file(saveGameDir+"/spiNNnetwork", 'w')
        components = {'spiNNpopulations': spiNNpopulations, 'spiNNprojections': spiNNprojections, 'Nindividuals': Nindividuals}
        cPickle.dump(components, f)
        f.close()
        #save neat networks
        fileName = saveGameDir+"/neatPop"
        neatPop.Save(fileName)
        return

    def initialize(self):

        #initiate population with fully connected networks without hidden neurons and run first generation
        # ! CAVEAT the neat algorithm apparantly outputs a slightly variable number of individuals !
        spiNNpopulations, spiNNprojections, Nindividuals, neatPop = self.Neat.initialization(Ninputs=12,Noutputs=4)

        fitness = np.empty((0))
        j=0
        while len(fitness) ==0:
        
            #create a saveGame directory
            saveGameDir = self.getDir()
            os.mkdir(saveGameDir)
            #save networks
            self.saveNets(spiNNpopulations, spiNNprojections, Nindividuals, neatPop, saveGameDir)
            #save figure of spiNNaker network
            visualization.visualize_spiNN_NN(copy.copy(spiNNprojections),saveGameDir+"/spiNNakerNetwork",size=None)
            #run simulation
            sim = interface17.interface(spiNNpopulations, spiNNprojections, Nindividuals, self.NFrames, self.repeats, saveGameDir)
            
            #wait until all pacman game results are collected
            while True:
                error = sim.getError()#@Alexander: delete this line, for debugging purposes only
                
                if sim.getSimFinished() and not(sim.getErrorCatched()):
                    fitness = sim.getScores() #will the outer While statement
                    print "gotfirstfitness"
                    break
                elif not(sim.getSimFinished()) and sim.getErrorCatched():
                    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                    print "error occured, try this generation (0) again (%d)" %(j)
                    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                    j+=1
                    self.Nerrors += 1            
                    break

        print "////////// generation %d \\\\\\\\\\"%(0)  
        self.avg_scores.append(int(np.average(fitness)))
        print "avg scores = ", self.avg_scores
        self.max_scores.append(int(np.max(fitness)))    
        print "max scores = " , self.max_scores
        for x in fitness:
            print x, "%i +-%i; " %(int(np.average(x)), int(np.std(x)))
        

        if np.min(fitness)<0:#make all fitnesses nonnegative
            fitness = fitness + np.abs(np.min(fitness))
        #take average along axis=1, in case repeated plays by one individual
        fitness = np.average(fitness,axis=1)
        
        return neatPop, fitness
        
    def runLoop(self,neatPop,fitness,maxGens):
        #start evolutionary loop
        for i in range(1,maxGens):
            
            self.startTime = time.time()#used to time duration of entire run
            # create next population
            spiNNpopulations, spiNNprojections, Nindividuals, neatPop = self.Neat.breed(neatPop, fitness)

            fitness = np.empty((0))
            j=0
            while len(fitness) ==0:
        
                saveGameDir = self.getDir()
                os.mkdir(saveGameDir)
                self.saveNets(spiNNpopulations, spiNNprojections, Nindividuals, neatPop, saveGameDir)
                visualization.visualize_spiNN_NN(copy.copy(spiNNprojections),saveGameDir+"/spiNNakerNetwork",size=None)
                print "saving nets and figure took %f s" %(time.time()-self.startTime)
                sim = interface17.interface(spiNNpopulations, spiNNprojections, Nindividuals, self.NFrames, self.repeats, saveGameDir)
                time.sleep(30)
                while True:
                    error = sim.getError()#@Alexander: delete this line, for debugging purposes only
                    time.sleep(2)
                    if sim.getSimFinished() and not(sim.getErrorCatched()):
                        fitness = sim.getScores() #breaks the outer While statement
                        print "got fitnesses"
                        break
                    elif not(sim.getSimFinished()) and sim.getErrorCatched():
                        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        print "error occured, try this generation (%d) again (%d)" %(i,j)
                        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                        j+=1
                        self.Nerrors +=1
                        break
                    
            visualization.savePlotResults("/home/alex/Documents/saveGames/"+ self.experimentName)
            print "////////// generation %d \\\\\\\\\\"%(i)  
            self.avg_scores.append(int(np.average(fitness)))
            print "avg scores = ", self.avg_scores
            self.max_scores.append(int(np.max(fitness)))    
            print "max scores = " , self.max_scores
            for x in fitness:
                print x, "%i +-%i; " %(int(np.average(x)), int(np.std(x)))

                    
            if np.min(fitness)<0:#make all fitnesses positive
                fitness = fitness + np.abs(np.min(fitness))

            #take average along axis=1, in case repeated plays by one individual
            fitness = np.average(fitness,axis=1)            
            print "running generation took %f s" %(time.time()-self.startTime)
        return 
        
