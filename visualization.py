# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 15:13:50 2016

@author: alex
"""

import numpy as np
import pygraphviz as pgv
import MultiNEAT as NEAT

import cPickle
import pacmanGame
import graphicsDisplay
import glob
import natsort
import simplejson
import matplotlib.pyplot as plt
import time

def replayPacman(recordedGame,frameTime=0.0):
    
    f = open(recordedGame)
    try: recorded = cPickle.load(f)
    finally: f.close()
    #recorded['display'] = args['display']
    #print recorded
    pacmanGame.replayGame(recorded['layout'],recorded['actions'],display=graphicsDisplay.PacmanGraphics(frameTime=frameTime))
    print "score = %d" %(recorded['score'])    
    return

def replayPacmanGeneration(folder,frameTime=0.0):
    games = glob.glob(folder+'/recorded*')
    
    games=natsort.natsorted(games)
    replayScores = list()
    
    for recordedGame in games:
        idx = recordedGame.find("NN")
        print recordedGame[idx:idx+4] + " replaying"
        f = open(recordedGame)
        try: recorded = cPickle.load(f)
        finally: f.close()
        #recorded['display'] = args['display']
        #print recorded
        pacmanGame.replayGame(recorded['layout'],recorded['actions'],display=graphicsDisplay.PacmanGraphics(frameTime=frameTime))
        replayScores.append(recorded['score'])
        print "scores = " , replayScores

def plotResults(folder):

    scores = list()
    maxScores= list()
    avgScores = list()
    generationTimeStamp = list()
    
    experiment = glob.glob(folder+'/2016*')
    experiment = natsort.natsorted(experiment)    
    
    for generation in experiment:
        try:
            with open(generation+"/scores","r") as openfile:
                allScores = simplejson.load(openfile)
                scores.append(allScores['scores'])
                maxScores.append(allScores['max_score'])
                avgScores.append(allScores['avg_score'])
                print generation[generation.find('2016'):], " max: ", allScores['max_score']
        except IOError:
            continue
        generationTimeStamp.append(generation[generation.find('2016'):])
    print "maxScores : " , maxScores    
    plt.plot(range(len(maxScores)),maxScores,label="max",color='r')#range(len(scores)),avgScores
    plt.plot(range(len(avgScores)),avgScores,label="average",color='black')#
    plt.title("pacman scores")        
    plt.legend(loc='lower right')
    plt.xlabel('Generation')
    plt.ylabel('Score')

def savePlotResults(folder):
    scores = list()
    maxScores= list()
    avgScores = list()
    generationTimeStamp = list()
    
    experiment = glob.glob(folder+'/2016*')
    experiment = natsort.natsorted(experiment)    
    
    for generation in experiment:
        try:
            with open(generation+"/scores","r") as openfile:
                allScores = simplejson.load(openfile)
                scores.append(allScores['scores'])
                maxScores.append(allScores['max_score'])
                avgScores.append(allScores['avg_score'])
        except IOError:
            continue
        generationTimeStamp.append(generation[generation.find('2016'):])
#    print "maxScores : " , maxScores    
    fig=plt.figure()
    plt.plot(range(len(maxScores)),maxScores,label="max",color='r')#range(len(scores)),avgScores
    plt.plot(range(len(avgScores)),avgScores,label="average",color='black')#
    plt.title("pacman scores")        
    plt.legend(loc='lower right')
    plt.xlabel('Generation')
    plt.ylabel('Score')
    fig.savefig(folder+'/resultLive.svg',bbox_inches='tight')   # save the figure to file
    plt.close(fig)
    time.sleep(2)
    
def plotResultsGeneration(folder):
    scores = list()
    
    #generation = glob.glob(folder+'/*')
    #generation = natsort.natsorted(experiment)  
    
    with open(folder+"/scores","r") as openfile:
        allScores = simplejson.load(openfile)
    scores = allScores['scores']
    maxScore = allScores['max_score']
    avgScore = allScores['avg_score']

    fig = plt.figure()
    plt.plot(range(len(scores)),scores,'*')
    plt.hlines(avgScore,0,len(scores),label="average")
    plt.xlabel("individual #")
    plt.ylabel("score")
    plt.legend()
    title = folder[folder.find('saveGames/')+9:]
    plt.title(title)


def plotInjectionDurations(folder):
    
    avgDurations = []
    stdDurations = []
    
    experiment = glob.glob(folder+'/*')
    experiment = natsort.natsorted(experiment)
    percentAbove20 = []
    
    for generation in experiment:
        try:
            durations = simplejson.load(open(generation+'/spiNN_injection_durations'),"r")
            avg = np.average(np.array(durations['injection_durations']))
            avgDurations.append(avg)
            std = np.std(np.array(durations['injection_durations']))
            stdDurations.append(std)
            p20 = len(np.where(np.array(durations['injection_durations'])>0.02)[0])/float(len(durations['injection_durations']))
            percentAbove20.append(p20)
        except IOError:
            pass
        
    N = len(avgDurations)
    fig = plt.figure()
    plt.plot(range(N),[1000*x for x in avgDurations],label='average(s)' )
    plt.plot(range(N),[x*1000+y*1000 for x,y in zip(avgDurations,stdDurations)],label='std',color='black')
    plt.plot(range(N),[x*1000-y*1000 for x,y in zip(avgDurations,stdDurations)],color='black')
    plt.plot(range(N),[x*100 for x in percentAbove20],'*',label='\% > 0.02s')
    plt.title('injection_durations')
    plt.legend()
    plt.xlabel('generation #')
    plt.ylabel('s or \%')

def plotAllSpikes(folder):
    plotInputSpikes(folder+'/spiNN_spiking_data_input')
    plotOutputSpikes(folder+'/spiNN_spiking_data_output',openPlot=True)
    plotPackageSpikes(folder+'/spiNN_saved_packages',openPlot=True)
    plt.title('spikes (input: blue, recorded output: green, package output : red )')
    
def plotInputSpikes(folder):
    inputs = np.load(open(folder))
    neuronIDs = inputs[0]
    spikeTimes = inputs[1]
    spikeTimes = (spikeTimes-spikeTimes[0])*1000#in ms
    fig = plt.figure()
    plt.plot(spikeTimes,neuronIDs,'*',color='blue')
    plt.title('input spikes')
    plt.xlabel('time (ms)')
    plt.ylabel('neuron ID')
    plt.ylim(-1,np.max(neuronIDs)+1)

def plotOutputSpikes(folder,openPlot=False):    
    outputs = np.load(open(folder))
#    neuronIDs = outputs[0]
#    spikeTimes = outputs[1]
#    spikeTimes = (spikeTimes-spikeTimes[0])*1000#in ms
    if not openPlot:
        fig = plt.figure()
        plt.plot(outputs[:,1],outputs[:,0],'*')
    if openPlot:
        plt.plot(outputs[:,1],outputs[:,0]+0.13,'*',color='green')
    plt.title('output spikes')
    plt.xlabel('time (ms)')
    plt.ylabel('neuron ID')
    plt.ylim(-1,np.max(outputs[:,0])+1)

def plotPackageSpikes(folder,openPlot=False):    
    packs = np.load(open(folder))
#    neuronIDs = outputs[0]
#    spikeTimes = outputs[1]
#    spikeTimes = (spikeTimes-spikeTimes[0])*1000#in ms
    if not openPlot:
        fig = plt.figure()
        plt.plot(packs[:,1],packs[:,0],'*')
    if openPlot:
        plt.plot(packs[:,1],packs[:,0]+0.26,'*',color='red')
    plt.title('received package spikes')
    plt.xlabel('time (ms)')
    plt.ylabel('neuron ID')
    plt.ylim(-1,np.max(packs[:,0])+1)
    
def plotRandomNet(pop):
    i = np.random.randint(0,len(pop.Species))
    spec = pop.Species[i]
    i = np.random.randint(0,len(spec.Individuals))
    ind = spec.Individuals[i]
    net=NEAT.NeuralNetwork()
    ind.BuildPhenotype(net)
    visualize(net,"latestRandomPick")
    
def visualize(net, filename):
    

    """ Visualize the network, stores in file. """
    
    inputs = net.NumInputs()
    outputs = net.NumOutputs()
    
    # Some settings
    node_dist = 1
    cm = get_cm(net)
    print cm
    # Clear connections to input nodes, these arent used anyway

    G = pgv.AGraph(directed=True)
    mw = np.nanmax(abs(cm))
        
    for i in range(cm.shape[0]):
        G.add_node(i)
        #t = self.node_types[i].__name__
        G.get_node(i).attr['label'] = '%d' % (i)
        for j in range(cm.shape[1]):
            w = cm[i,j]
            if abs(w) > 0.01:
                G.add_edge(j, i, penwidth=abs(w)/mw*4, color='blue' if w > 0 else 'red')
                

                
                
    for n in range(inputs):
        pos = (node_dist*n, 0)
        G.get_node(n).attr['pos'] = '%s,%s!' % pos
        G.get_node(n).attr['shape'] = 'doublecircle'
        G.get_node(n).attr['fillcolor'] = 'steelblue'
        G.get_node(n).attr['style'] = 'filled'
    for i,n in enumerate(range(inputs,inputs+outputs)):
        pos = (node_dist*i, -node_dist * 5)
        G.get_node(n).attr['pos'] = '%s,%s!' % pos
        G.get_node(n).attr['shape'] = 'doublecircle'
        G.get_node(n).attr['fillcolor'] = 'tan'
        G.get_node(n).attr['style'] = 'filled'
    
    G.node_attr['shape'] = 'circle'

    prog = 'dot'
    G.draw(filename, prog=prog)

            
def get_cm(net):
    cm = np.zeros((len(net.neurons), len(net.neurons)))
    cm.fill(np.nan)
    for conn in net.connections:
        cm[conn.target_neuron_idx, conn.source_neuron_idx] = conn.weight
    
    return cm
    
def visualise_neatPop_as_spiNN_NN(neatPop, filename, size=None): 
    import neatPart1    
    tempNeat = neatPart1.Neat()
    allStructures, allConnections = tempNeat.extract_connectivity(neatPop)
    spiNN_network, spiNNproj = tempNeat.convert_to_spiNN(allConnections,allStructures)
    visualize_spiNN_NN(spiNNproj,filename,size)
    
def visualize_spiNN_NN(spiNN_projections, filename, size=None):    
    """
    Projections contains the projections that can be given to spiNNaker:
                Projections = [proj_input_hidden,
                           proj_input_output,
                           proj_hidden_hidden,
                           proj_hidden_output,
                           proj_output_hidden]
    size = Number of lines from each proj used for plotting, 
            if not all projections used the last plotted network is likely incomplete !                           
    """
    if size == None:
        projections = spiNN_projections
    else:
        projections = [spiNN_projections[0][:size], 
                       spiNN_projections[1][:size],
                       spiNN_projections[2][:size],
                       spiNN_projections[3][:size],
                       spiNN_projections[4][:size]]
                  
    #get the number of neurons per layer
    In = max(cmax([x[0] for x in projections[0]]),cmax([x[0] for x in projections[1]])) + 1
    Out = max(cmax([x[1] for x in projections[1]]),cmax([x[1] for x in projections[3]]),cmax([x[0] for x in projections[4]]) ) + 1
    Hidden = max(cmax([x[1] for x in projections[0]]),cmax([x[0] for x in projections[2]]),cmax([x[1] for x in projections[2]]),cmax([x[1] for x in projections[4]])) + 1
    
    In, Out, Hidden = int(In), int(Out), int(Hidden)
              
    cm = get_cm_from_proj(projections,In,Out,Hidden)
    
    node_dist = 1
    
    G = pgv.AGraph(directed=True)
    mw = np.nanmax(abs(cm))
        
    for i in range(cm.shape[0]):
        G.add_node(i)
        #t = self.node_types[i].__name__
        G.get_node(i).attr['label'] = '%d' % (i)
        for j in range(cm.shape[1]):
            w = cm[i,j]
            if abs(w) > 0.01:
                G.add_edge(j, i, penwidth=abs(w)/mw*4, color='blue' if w > 0 else 'red')
                

                
                
    for n in range(In):
        pos = (node_dist*n, 0)
        G.get_node(n).attr['pos'] = '%s,%s!' % pos
        G.get_node(n).attr['shape'] = 'doublecircle'
        G.get_node(n).attr['fillcolor'] = 'steelblue'
        G.get_node(n).attr['style'] = 'filled'
    for i,n in enumerate(range(In,In+Out)):
        pos = (node_dist*i, -node_dist * 5)
        G.get_node(n).attr['pos'] = '%s,%s!' % pos
        G.get_node(n).attr['shape'] = 'doublecircle'
        G.get_node(n).attr['fillcolor'] = 'tan'
        G.get_node(n).attr['style'] = 'filled'
    
    G.node_attr['shape'] = 'circle'

    prog = 'dot'
    G.draw(filename, prog=prog)
    
def cmax(x):
    try:
        m = max(x)
    except ValueError as e:#in case of empty list (no projections)
        if e.message == "max() arg is an empty sequence":
            m = 0
        else:
            raise
    return m
    
def get_cm_from_proj(projections,In,Out,Hidden):
    #firstly each neuron in all three layers needs to get an unique number, firts input neurons, 
    #then output neurons, then hidden neurons
    #convert immutable tuples -necessary for spyNN- to np arrays
    for x in range(5):
        projections[x] = np.array( [np.array(y) for y in projections[x]])
    #increment all output neurons with Number of input neurons
    try:
        projections[1][:,1] += In
    except IndexError:
        pass    
    try:
        projections[3][:,1] += In
    except IndexError:
        pass    
    try:
        projections[4][:,0] += In
    except IndexError:
        pass    
    #increment all hidden neurons with Number of input neurons + output neurons
    try:
        projections[0][:,1] += In+Out    
    except IndexError:
        pass    
    try:
        projections[2][:,0] += In+Out
    except IndexError:
        pass    
    try:
        projections[2][:,1] += In+Out
    except IndexError:
        pass    
    try:
        projections[3][:,0] += In+Out
    except IndexError:
        pass    
    try:
        projections[4][:,1] += In+Out
    except IndexError:
        pass    
    #build connection matrix    
    NNeurons = In+Out+Hidden    
    cm = np.zeros((NNeurons, NNeurons))
    cm.fill(np.nan)
    for p in projections:
        for conn in p:
            cm[conn[1],conn[0]]=conn[2]
    return cm
