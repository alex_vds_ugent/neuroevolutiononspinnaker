# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 17:02:45 2016

@author: alex

interface between spiNNaker and Pacman

"""

import numpy as np
import time
import pynnPart9b
import threading
import pacmanGame
import layout
import searchAgents
import textDisplay
import simplejson
import random
import glob

class interface:
    
    def __init__(self,spiNNpopulations, spiNNprojections, Nindividuals, NFrames, repeats, saveGameDir):


        timescalefactor=3
        
        self.e = 0
        self.errorCatched = False
        
        self.timebin=0.2# in s, interval between spinnaker input/output updates #what timebin makes sense?-must be small relative to pacman framerate
        self.runtime = NFrames*900*repeats/timescalefactor #duration (ms) of simulation on spiNNaker board, make sure it provides sufficient time for all game simulations
#        self.runtime=13000
        self.Nindividuals = Nindividuals
        print "input nneurons, hidden neurons, output neurons :" 
        print spiNNpopulations
        print "total number of projections: %d" %(sum([len(x) for x in spiNNprojections]))
        
                
        self.saveGameDir = saveGameDir
        self.NFrames = NFrames
        #setup spiNNaker board simulation
        self.sim = pynnPart9b.SpinnSimulation()
        self.sim.setup_populations(spiNNpopulations)
        print 'pop setup'
        self.sim.setup_projections(spiNNprojections)
        print 'proj setup'        
        self.sim.setup_live_spike_connections()
        print 'live conn setup'

        self.setSimFinished(False)
#        self.repeatN = 0#keeps track of wich repeat currently playing
                
        #pacMan game settings
#        self.maxGameTime = playTime#max time pacman can play (s), ToDo: link this with spiNNaker simulation duration
        self.ghosts  = []
        self.display = textDisplay.NullGraphics(speed=0.25)#'speed'=interFrameTime in seconds|change to textDisplay.NullGraphics textDisplay.PacmanGraphics or graphicsDisplay.PacmanGraphics to see pacman playing
        self.numGames= 1
        self.record  = False
        ###self.lay_out  = layout.getLayout("/home/alex/Gits/customPacman/layouts/swuare.lay")        
    
        #create list with random layout instances
        width = 6
        height = 6
        layoutBaseName = "mediumClassic"#"halterMazeSparse"#"simpleMaze2Sparse"#"bidirHor"#"square36"#"bigDottedSquare"#
        mazeList = glob.glob("/home/alex/Gits/customPacman/layouts/"+layoutBaseName+"X*")
        
        self.layouts = []
        for i in range(repeats):
#            self.layouts.append([layout.getLayout("/home/alex/Gits/customPacman/layouts/"+layoutBaseName+"X"+str(random.randint(1,width))+"Y"+str(random.randint(1,height))+".lay") for x in range(self.Nindividuals) ] )
            self.layouts.append([layout.getLayout("/home/alex/Gits/customPacman/layouts/"+layoutBaseName+".lay") for x in range(self.Nindividuals) ] )
#            self.layouts.append([layout.getLayout(random.sample(mazeList,1)[0]) for x in range(self.Nindividuals)])
        #create list with pacman instances        
        self.pac_mans  = [searchAgents.NN_Agent() for x in range(self.Nindividuals)] #searchAgents.NN_Agent()        
#        self.pac_mans  = [searchAgents.ClosestDotSearchAgent() for x in range(self.Nindividuals)] #searchAgents.NN_Agent()        
        #create list with games using pacman instances
        self.gamesList = []
        for i in range(repeats):
            self.gamesList.append( [pacmanGame.runGames(lay_out, pacman, self.ghosts, self.display, self.numGames, self.record) for pacman, lay_out in zip(self.pac_mans,self.layouts[i])] ) 
        #scores will be returned as the fitness
        self.scores=np.empty((self.Nindividuals,repeats))
        self.scores.fill(np.nan)        

        self.spiNNSimFinished = False
        self.lock1=threading.Lock()
        
        #prepare spiNN thread                  
        w = threading.Thread(target=self.simulationThread)
        #start thread
        w.start()
        #initialize games of repeat 1
#        for x in self.gamesList[0]:
#            x.run()        
        
        #wait until spiNNaker sim is started
        while True:
            if self.sim.isStarted():
                break        
            
        for repeatN in range(repeats):
            time0 = time.time()
            #initialize games of this repeat
            for x in self.gamesList[repeatN]:
                x.run()
            print "initializing games took %f s" %(time.time()-time0)
            for x in range(NFrames):
            #####get pacman inputs and feed to spiNNaker
                time1 = time.time()            
                pacmanPositions = [self.gamesList[repeatN][X].state.getPacmanPosition() for X in range(self.Nindividuals)]
                wallPositions = [self.gamesList[repeatN][X].state.getWalls() for X in range(self.Nindividuals)]
                dotPositions = [self.gamesList[repeatN][X].state.getFood() for X in range(self.Nindividuals)]
                if x!=0:
                    newLastMoves = [self.gamesList[repeatN][X].moveHistory[-1][1] for X in range(self.Nindividuals)]#get last moves
                    for idx, x in enumerate(newLastMoves):#only update last move if not == stop
                        if x != 'Stop':
                            lastMoves[idx] = x
                else:
                    lastMoves = ["dummy" for X in range(self.Nindividuals)]
                print 'lastMoves=', lastMoves
                active_neurons = self.get_activeNeurons(pacmanPositions,dotPositions,wallPositions,lastMoves)#contains indices of active neurons, (array([ , , ,]))
                self.sim.set_spikes_received(False)
                self.sim.put_spikes(active_neurons)
                print "updating active_neurons costs %f s"%(time.time()-time1)        
            #####get spiNNaker outputs and feed to pacman        
                #wait til spinnaker outputs received
                time1b = time.time()
                while True:
                    if self.sim.get_spikes_received():
                        break
                print "~~~~~~~getting output from spiNNaker costs %f s~~~~~~~"%(time.time()-time1b)                        
                time2=time.time()
                spikes = self.sim.get_spikes()
                self.extract_decision(spikes)
                print "<<<<<<updating DECISION costs %f s>>>>>>"%(time.time()-time2)
            #####update pacman games
                time3 = time.time()
                #wait for signal that next decision is available
                for x in self.gamesList[repeatN]:
                    x.nextFrame()
                print "-----updating frames costs %f s----"%(time.time()-time3)
                print "°°°°°°Complete cycle took %f s °°°°°°°"%(time.time()-time1)
            #store scores
            for idx, x in enumerate(self.gamesList[repeatN]):
                x.finish()
                self.lock1.acquire()
                self.scores[idx][repeatN] = x.state.getScore()
                self.lock1.release()
            time.sleep(0.5)    
            

                
        #write out scores
        print "saving scores"
        self.saveScores()
        print "scores saved"
        #write out savegames    
        time4=time.time()
        for z in range(repeats):
            for idx, x in enumerate(self.gamesList[z]):
                x.saveGame(self.layouts[z][idx],x.moveHistory,x.state.getScore(), idx,self.saveGameDir)
            time.sleep(1)#wait 1 sec, otherwise savegames could have same name and be overwritten
        print "saving game history took %f s"%(time.time()-time4)
        #close ConnectionListener thread
        for x in threading.enumerate(): 
            if 'ConnectionListener' in x.__str__():
                x.close()
                x.join(5)

        #wait untill spiNNaker is finished        
        while True:
            if self.spiNNSimFinished:
                self.setSimFinished(True)#give signal to outer evoloop
                break
        
    def getError(self):
        return self.e

    def getScores(self):
        self.lock1.acquire()
        scores=self.scores.copy()
        self.lock1.release()
        return scores
        
    def extract_decision(self,spikes):
        #loop per 4 items in 'spikes', !assuming 4 output nodes! , pass movement decision to pacman game
                
        for out1, out2, out3, out4, pacman, i in zip(spikes[0::4],spikes[1::4],spikes[2::4],spikes[3::4],self.pac_mans,range(len(self.pac_mans))): 
            
            #collect firing rates (fr)
            up_fr = out1/self.timebin
            right_fr = out2/self.timebin
            down_fr = out3/self.timebin
            left_fr = out4/self.timebin
            spike_fr = [up_fr, right_fr, down_fr, left_fr]    
#            if i==0:
#                print "[up_fr %f, right_fr %f, down_fr %f, left_fr %f]" %(up_fr, right_fr, down_fr, left_fr)
#            print "up_fr : ", up_fr, "\n", "right_fr : ", right_fr, "\n", "down_fr : ", down_fr, "\n", "left_fr : ", left_fr, "\n"
            thresHold = 7 # in Hz, firing rate threshold to execute decision, if none reaches threshold pacman stands still            
            if max(spike_fr)>self.timebin*thresHold:
                maxi = spike_fr.index(max(spike_fr))
                if maxi == 0:
                    pacman.setAction("up")
#                    print "action set to north"
                elif maxi == 1:
                    pacman.setAction("right")
#                    print "action set to east"
                elif maxi == 2:
                    pacman.setAction("down")
#                    print "action set to south"
                elif maxi == 3:
                    pacman.setAction("left")
#                    print "action set to west"
            else:#decision threshold not reached
                pacman.setAction("stop")
#                print "action set to stop"
            
        
    def get_activeNeurons(self,pacmanPositions,dotPositions,wallPositions,lastMoves):
        activeNeurons = list()
        for pp, dp, wp, lm in zip(pacmanPositions,dotPositions,wallPositions,lastMoves):

            if  dp[pp[0]][pp[1]+1] :#upTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if dp[pp[0]+1][pp[1]]:#rightTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if dp[pp[0]][pp[1]-1]:#downTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if dp[pp[0]-1][pp[1]]:#leftTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)         

            if  wp[pp[0]][pp[1]+1] :#upTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if wp[pp[0]+1][pp[1]]:#rightTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if wp[pp[0]][pp[1]-1]:#downTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if wp[pp[0]-1][pp[1]]:#leftTile
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)

            if lm == "South":#lastposition = up
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)                
            
            if lm == "West":#last position = right
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)
            
            if lm == "North":#lastposition = down
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)
            
            if lm == "East":#last position = Left
                activeNeurons.append(1)
            else:
                activeNeurons.append(0)                
        #activeNeurons = binary list (in)active neurons. [upN1,rightN1,downN1,leftN1, upN2,rightN2, ...]
        activeNeuronIndices = np.where(np.array(activeNeurons)==1)
#        print activeNeuronIndices
        return list(activeNeuronIndices[0])
    
    def setSimFinished(self,boolean):
        self.simFinished = boolean
    
    def getSimFinished(self):
        return self.simFinished

    def setErrorCatched(self,boolean):
        self.errorCatched = boolean
        
    def getErrorCatched(self):
        return self.errorCatched
        
    def saveScores(self):
        i=0
        while i<30:
            if not np.isnan(np.sum(self.getScores())):#check if nan values left
                scoresDictionary = {'scores': [list(x) for x in self.scores], 'max_score': int(np.max(self.scores)), 'min_score': int(np.min(self.scores)), 'avg_score': int(np.average(self.scores))}
                with open(self.saveGameDir+"/scores","w") as f:
                    simplejson.dump(scoresDictionary,f)
                return
            else:
                i=i+1
                print "trying to save scores ...."
                time.sleep(1)
                
        raise Exception ("failed to write scores file due to a NaN value/timeout")
        
    def saveSpikes(self):
#        recorded_output_spikes=self.sim.get_recorded_output_spikes()
#        totalInputsSent = self.sim.get_total_spikes_sent()
        lateSpikes= self.sim.get_late_spikes()        
        spikesDictionary = {'missedSpikes':lateSpikes}#{'totalInputsSent':totalInputsSent, 'recorded_output_spikes':[list(x) for x in recorded_output_spikes]}
        with open(self.saveGameDir+'/spiNN_spiking_data',"w") as openF:
            simplejson.dump(spikesDictionary,openF)
    
    def saveInjectionDurations(self):
        durations = self.sim.get_injection_durations()
        with open(self.saveGameDir+'/spiNN_injection_durations',"w") as openF:
            simplejson.dump({"injection_durations":durations},openF)        

    def simulationThread(self):
        
        
        #run simulation on spiNNaker board
        try:
            self.sim.run_sim(self.runtime)
        except Exception as e:
            #ExecutableFailedToStopException:
            print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            self.e=e
            print "         exception caught"
            print "e.message :", e.message
            self.sim.end_sim()#closes spinnaker, note that self.simFinished remains False
            self.setErrorCatched(True)
            return
            
        #save data
        print "getting spikes"            
        self.saveSpikes()
        #close spinnaker 
        print "closing spinnaker"
        self.sim.end_sim()        
        #save injection_durations
#        self.saveInjectionDurations()
        self.spiNNSimFinished = True
        print "@@@@@@@@@@@@@@@THE SPINNAKER SIM IS ENDED@@@@@@@@@@@@@@@"