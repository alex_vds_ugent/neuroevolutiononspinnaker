# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 12:56:47 2016

@author: alex

pynnPart 9 (serialPacman)

setup network(s) in pyNN and run simulation
"""

import spynnaker.pyNN as Frontend
import spynnaker_external_devices_plugin.pyNN as ExternalDevices
from spynnaker_external_devices_plugin.pyNN.connections.spynnaker_live_spikes_connection import \
    SpynnakerLiveSpikesConnection
import time
import random
from threading import Condition
import numpy as np
import threading

    
class SpinnSimulation:
    def __init__(self):
        self.output_spikes = list()
        Frontend.setup(timestep=1.0, min_delay=1.0, max_delay=144.0)
        #self.weight_to_spike = 2.0
        #self.delay = 1
        #self.inhibitory_weight = -1 * self.weight_to_spike
        
        self.cell_params_lif = {'cm': 0.25,
                                'i_offset': 0.0,
                                'tau_m': 20.0,
                                'tau_refrac': 2.0,
                                'tau_syn_E': 5.0,
                                'tau_syn_I': 5.0,
                                'v_reset': -70.0,
                                'v_rest': -65.0,
                                'v_thresh': -50.0
                                }
        
        self.cell_params_spike_injector = {
            'port': 12345
        }
        self.print_condition = Condition()
        self.received_spikes = np.empty(())        
        #create a lock that prevents different threads from accessing a resource at the same time
        self.lock1=threading.Lock()
        self.lock2=threading.Lock()
        
        self.simStarted = False
        self.simEnded = False
        self.active_neurons = False#prevents send_spike from running until self.active_neurons replaced with data
        self.active_neurons_put = False
        self.spikesReceived = False
        self.lateSpikes=0
        self.lastReceiveDelay=0
        
        self.inputTimes = []
        self.inputRec = []
        self.simStartTime = -10000 #used for printing time to terminal
        self.packages = []
        self.i=0#used for timing spike receiver
    def setup_populations(self,populations):
        """
        populations = [Ninputs,Nhidden,Noutput]
        """
        # Create injection population for sending live spikes
        self.injector_pop = Frontend.Population(populations[0], ExternalDevices.SpikeInjector, self.cell_params_spike_injector,
                                                label='injector_pop')
        #don't make an empty population
        if not populations[1] == 0:
            self.hidden_pop = Frontend.Population(populations[1], Frontend.IF_curr_exp, self.cell_params_lif, label='hidden_pop')                                        
        
        self.output_pop = Frontend.Population(populations[2], Frontend.IF_curr_exp, self.cell_params_lif, label='output_pop')        
        # record spikes
#        self.output_pop.record()
        # resize self.received_spikes to the size of the output layer
        
        self.received_spikes = np.zeros((populations[2]))        
        self.Noutputs = populations[2] #used in clear_spikes()
        # Activate the sending of live spikes
                
        while True:
            try:
                port = random.randint(2000,65000)
                ExternalDevices.activate_live_output_for(self.output_pop,port=port)
                break
            except KeyError:
                print "port%d seems to be in use already, let's try another one" %(port)
                pass
            
        #ExternalDevices.activate_live_output_for(self.output_pop)#, database_notify_host="localhost", database_notify_port_num=19996
                                             
    def setup_projections(self,projections):
        """
                spiNN_projections = [proj_input_hidden,
                             proj_input_output,
                             proj_hidden_hidden,
                             proj_hidden_output,
                             proj_output_hidden]

        """
        #make random weights, make sure there are more than needed for allToAll connectivity (even if only fraction used) 
        #weights1 = [np.random.uniform(0,1) for x in range(1000)]
        #weights2 = [np.random.uniform(0,1) for x in range(120)]
        """
        self.proj_input_hidden = Frontend.Projection(self.injector_pop, self.hidden_pop,\
            Frontend.FromListConnector(projections[0]))
            
        self.proj_hidden_output = Frontend.Projection(self.hidden_pop, self.output_pop, \
            Frontend.FromListConnector(projections[3]))        
        """ 
        
        if len(projections[0]) > 0 :
            #make excitatory and inhibitory proj list
            proj_exc = [x for x in projections[0] if x[2]>0]
            proj_inh = [x for x in projections[0] if x[2]<0]
            if len(proj_exc) > 0:
                self.proj_input_hidden_exc = Frontend.Projection(self.injector_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_exc),target="excitatory")
    
            if len(proj_inh) > 0:
                self.proj_input_hidden_inh = Frontend.Projection(self.injector_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_inh),target="inhibitory")

        if len(projections[1]) >0:
            proj_exc = []
            proj_inh = []            
            #make excitatory and inhibitory proj list
            proj_exc = [x for x in projections[1] if x[2]>0]
            proj_inh = [x for x in projections[1] if x[2]<0]            
            if len(proj_exc) > 0:
                self.proj_input_output_exc = Frontend.Projection(self.injector_pop, self.output_pop,\
                    Frontend.FromListConnector(proj_exc),target="excitatory")
            if len(proj_inh) > 0:
                self.proj_input_output_inh = Frontend.Projection(self.injector_pop, self.output_pop,\
                    Frontend.FromListConnector(proj_inh),target="inhibitory")
        
        if len(projections[2]) > 0:        
            proj_exc = []
            proj_inh = []            
            #make excitatory and inhibitory proj list
            proj_exc = [x for x in projections[2] if x[2]>0]
            proj_inh = [x for x in projections[2] if x[2]<0]                        
            if len(proj_exc) > 0:
                self.proj_hidden_hidden_exc = Frontend.Projection(self.hidden_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_exc),target="excitatory")
            if len(proj_inh) > 0:
                self.proj_hidden_hidden_inh = Frontend.Projection(self.hidden_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_inh),target="inhibitory")

        if len(projections[3]) > 0:
            proj_exc = []
            proj_inh = []            
            #make excitatory and inhibitory proj list
            proj_exc = [x for x in projections[3] if x[2]>0]
            proj_inh = [x for x in projections[3] if x[2]<0]        
            if len(proj_exc) > 0:
                self.proj_hidden_output_exc = Frontend.Projection(self.hidden_pop, self.output_pop, \
                    Frontend.FromListConnector(proj_exc),target="excitatory")
            if len(proj_inh) > 0:
                self.proj_hidden_output_inh = Frontend.Projection(self.hidden_pop, self.output_pop, \
                    Frontend.FromListConnector(proj_inh),target="inhibitory")
        
        if len(projections[4]) > 0:
            proj_exc = []
            proj_inh = []            
            #make excitatory and inhibitory proj list
            proj_exc = [x for x in projections[4] if x[2]>0]
            proj_inh = [x for x in projections[4] if x[2]<0]            
            if len(proj_exc) > 0:
                self.proj_output_hidden_exc = Frontend.Projection(self.output_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_exc),target="excitatory")
            if len(proj_inh) > 0:
                self.proj_output_hidden_inh = Frontend.Projection(self.output_pop, self.hidden_pop,\
                    Frontend.FromListConnector(proj_inh),target="inhibitory")
        
#        print "projections[2] (hiddenhidden) = ",projections[2]
#        print "projections[4] (outputhidden) = ",projections[4]
           
    def send_spikes(self,label,sender):
        #change status to running!
        if not self.simStarted:
            self.simStarted = True
            self.simStartTime = time.time()
#        self.totalSpikesSent = 0
#        time.sleep(1)
        while not self.simEnded:      
            if self.active_neurons_put:
                print "################### started sending spikes###########################"
                self.initTime = time.time()
                #fire active neurons at 50Hz
#                self.durationCollector = []
                time1 = 0
                for x in range(3):
                    sender.send_spikes(label,self.active_neurons)
                self.active_neurons_put = False
                """
                while time.time()-self.initTime<=0.1:  
        #            time.sleep(0.02)
        #                print "-------going to send spike from neurons", self.active_neurons
                    if time.time()-time1>=0.04:
                        time1=time.time()
                        self.lock2.acquire()            
        #                for neuron_id in range(0,300):#self.active_neurons[0]:
                        sender.send_spikes(label,self.active_neurons)
        #                sender.send_spikes(label,range(0,300))
#                        for x in self.active_neurons:
#                            self.inputRec.append(x)
#                            self.inputTimes.append(time.time())
#                        duration = time.time()-time1
        #                print "sending all spikes in self.active_neurons took %f s" %(time.time()-timePre)                
        #                self.totalSpikesSent = self.totalSpikesSent+len(self.active_neurons)
                        self.lock2.release()
#                        self.durationCollector.append(duration)
                    """
                print "################### finished sending spikes after %f s ###############" %(time.time()-self.initTime)            
                time.sleep(0.1)
                while time.time()-self.initTime<0.6:
                    if time.time()-self.initTime-self.lastReceiveDelay>0.1:
                        self.set_spikes_received(True)#signal for listeningThread
                        print "seems like no more spikes are coming %f s after started sending spikes"%(time.time()-self.initTime)
                        break
                print "-----------------------------------------------im past the while loop"
                #stop waiting anyway, enough time has passed: 
                if not self.get_spikes_received():
                    print " --------------------------------!!! I'm tired of waiting, lets move on !!!"
                    self.set_spikes_received(True)                                        
            
    def receive_spikes(self, label, arrival_time, neuron_ids):
        #for neuron_id in neuron_ids:
            #print "Received spike at time {} from {}-{}".format(arrival_time, label, neuron_id)
#        if self.i == 0:
#            self.startTime=time.time()+arrival_time*0.001
#            self.i=1
        if len(neuron_ids)>2:
            print "package received at timestamp {} with {} neuron_ids " .format(arrival_time,len(neuron_ids))
        else:
            print arrival_time,
#        print "running time = %f"%(time.time()-self.startTime)
        self.lastReceiveDelay=time.time()-self.initTime
        if len(neuron_ids)>3:
            print "time since started sending spikes = %f s"%(self.lastReceiveDelay) 
#        preTime = time.time()
        self.lock1.acquire()        
        for neuron_id in neuron_ids:#@Alexander: TODO: check if this loop can go faster
            self.received_spikes[neuron_id] += 1
#            self.packages.append([neuron_id,arrival_time])
        self.lock1.release()
#        print "recSp", self.received_spikes
#        print "receive_spikes took %f s" %(time.time()-preTime)
    
    def setup_live_spike_connections(self):

        # Set up the live connection for sending and receiving spikes    
        self.live_spikes_connection = SpynnakerLiveSpikesConnection(receive_labels=["output_pop"],send_labels=["injector_pop"])
        #self.live_spikes_connection = SpynnakerLiveSpikesConnection(send_labels=["injector_pop"])
        # Set up callbacks to occur at the start of simulation
        self.live_spikes_connection.add_start_callback("injector_pop",self.send_spikes)
        # Set up callbacks to occur when spikes are received
        self.live_spikes_connection.add_receive_callback("output_pop",self.receive_spikes)
        
    
    def get_spikes(self):
        self.lock1.acquire()
        spikes = np.copy(self.received_spikes)
        self.received_spikes = np.zeros((self.Noutputs)) #reset spike count
        self.lock1.release()
#        print "time = %f s" %(time.time()-self.simStartTime)
#        print "FR:", spikes
        return spikes        
        
    def put_spikes(self,activeNeurons):
        #count number of spikes that came too late
        self.lateSpikes = self.lateSpikes + sum(self.received_spikes)
        self.received_spikes = np.zeros((self.Noutputs)) #reset spike count
        self.lock2.acquire()
        #update which neurons are active
        self.active_neurons = activeNeurons
        self.lock2.release()
        self.active_neurons_put = True
    
    def set_spikes_received(self, boolean):
        self.spikesReceived = boolean

    def get_spikes_received(self):
        return self.spikesReceived
    
    def get_recorded_output_spikes(self):
        spikes = self.output_pop.getSpikes()
        return spikes
    
    def get_saved_packages(self):
        return self.packages
        
    def get_recorded_input_spikes(self):
        return self.inputRec, self.inputTimes
        
    def get_injection_durations(self):
        return self.durationCollector
        
    def get_total_spikes_sent(self):
        return self.totalSpikesSent
    
    def get_late_spikes(self):
        return self.lateSpikes
        
    def print_spikes(self):
        #print recorded spikes 
        print self.hidden_pop.getSpikes()
    
    def isStarted(self):
        if self.simStarted:        
            return True
        else:
            return False
            
    def run_sim(self, runtime):
        Frontend.run(runtime)
        
    def end_sim(self):
        #free port used by live connection
        self.live_spikes_connection.close()        
        # Clear data structures on spiNNaker to leave the machine in a clean state for future executions
        Frontend.end()

        self.simEnded = True